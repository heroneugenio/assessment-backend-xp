# Banco de Dados

***Criando banco e tabelas:***

- CREATE DATABASE `wjump_desafio` CHARACTER SET utf8 COLLATE utf8_general_ci;

- USE `wjump_desafio`;

- CREATE TABLE IF NOT EXISTS `categories` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(250) NOT NULL,
  `name` VARCHAR(250) NOT NULL,
  created_at timestamp null DEFAULT null, 
   updated_at timestamp null DEFAULT null, 
   deleted_at timestamp null DEFAULT null,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

- CREATE TABLE IF NOT EXISTS `products` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `sku` VARCHAR(250) NOT NULL,
  `name` VARCHAR(250) NOT NULL,
  `price` VARCHAR(250) NOT NULL,
  `description` VARCHAR(250) NOT NULL,
  `quantity` VARCHAR(100) DEFAULT 0,
  `image` VARCHAR(250),
   created_at timestamp null DEFAULT null, 
   updated_at timestamp null DEFAULT null, 
   deleted_at timestamp null DEFAULT null,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

- CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT UNSIGNED NOT NULL,
  `category_id` INT UNSIGNED NOT NULL,
  created_at timestamp null DEFAULT null, 
   updated_at timestamp null DEFAULT null, 
   deleted_at timestamp null DEFAULT null,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-ALTER TABLE product_categories ADD CONSTRAINT fk_product_category FOREIGN KEY (category_id) REFERENCES categories(id);

-ALTER TABLE product_categories ADD CONSTRAINT fk_category_product FOREIGN KEY (product_id) REFERENCES products(id);

***database.php***

```bash
<?php

use Illuminate\Database\Capsule\Manager;

$capsule = new Manager;

$capsule->addConnection([
                            'driver'    => 'mysql',
                            'host'      => 'localhost',
                            'database'  => 'banco_nome',
                            'username'  => 'root',
                            'password'  => 'sua_senha',
                            'charset'   => 'utf8',
                            'collation' => 'utf8_general_ci',
                            'prefix'    => '',
                        ]);

$capsule->setAsGlobal();
$capsule->bootEloquent();
```
***Subir servidor php***
```bash
php -S localhost:8000 -t public/
```


# Você quer ser um desenvolvedor Backend na Web Jump?
Criamos esse teste para avaliar seus conhecimentos e habilidades como desenvolvedor backend.

# O teste
O desafio é desenvolver um sistema de gerenciamento de produtos. Esse sistema será composto de um cadastro de produtos e categorias. Os requisitos desse sistema estão listados nos tópicos abaixo.
Não existe certo ou errado, queremos saber como você se sai em situações reais como esse desafio.

# Instruções
- O foco principal do nosso teste é o backend. Para facilitar você poderá utilizar os arquivos html  disponíveis no diretório assets
- Crie essa aplicação como se fosse uma aplicação real, que seria utilizada pelo WebJump
- Fique à vontade para usar bibliotecas/componentes externos
- Não utilize nenhum Framework, tais como Laravel, Symphony
- Seguir princípios **SOLID** 
- Utilize boas práticas de programação
- Utilize boas práticas de git
- Documentar como rodar o projeto
- Crie uma documentação simples comentando sobre as tecnologias, versões e soluções adotadas

# Requisitos
- O sistema deverá ser desenvolvido utilizando a linguagem PHP (de preferência a versão mais nova) ou outra linguagem se assim foi especificado para sua avaliação por nossa equipe.
- Você deve criar um CRUD que permita cadastrar as seguintes informações:
	- **Produto**: Nome, SKU (Código), preço, descrição, quantidade e categoria (cada produto pode conter uma ou mais categorias)
	- **Categoria**: Código e nome.
- Salvar as informações necessárias em um banco de dados (relacional ou não), de sua escolha

# Opcionais
- Gerar logs das ações
- Testes automatizados com informação da cobertura de testes
- Upload de imagem no cadastro de produtos

# O que será avaliado
- Estrutura e organização do código e dos arquivos
- Soluções adotadas
- Tecnologias utilizadas
- Qualidade
- Padrões PSR, Design Patterns
- Enfim, tudo será observado e levado em conta

# Como iniciar o desenvolvimento
- Fork esse repositório na sua conta do BitBucket.
- Crie uma branch com o nome desafio

# Como enviar seu teste
Envie um email para [carreira@webjump.com.br] com o link do seu repositório.

Se o seu repositório for privado, conceda acesso ao email [codereview@webjump.com.br].

Qualquer dúvida sobre o teste, fique a vontade para entrar em contato conosco.